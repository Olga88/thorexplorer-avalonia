﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

namespace ThorExplorer.Models
{
    public class Engine
    {
        public string EngineName { get; set; }

        public ObservableCollection<DataBase> DataBases { get;  } 
            = new ObservableCollection<DataBase>();

        public Engine()
        {
            DataBases = new ObservableCollection<DataBase>();
        }
    }
}
