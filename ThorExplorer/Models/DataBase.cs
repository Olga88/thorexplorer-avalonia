﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using ThorExplorer.Models;

namespace ThorExplorer.Models
{
    public class DataBase
    {
        public string DataBaseName { get; set; }
        
        public ObservableCollection<Indexx> Indices { get; }

        public DataBase()
        {
            Indices = new ObservableCollection<Indexx>();
        }
    }
}
