﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

namespace ThorExplorer.Models.ObjectsTable
{
    public class PersistentObject
    {
        public string Identifier { get; set; }
        public string Description { get; set; }
        public string Type { get; set; }
    }
}
