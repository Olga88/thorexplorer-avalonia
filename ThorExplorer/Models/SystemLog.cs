﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThorExplorer.Models
{
    public class SystemLog
    {
        public string TypeName { get; set; }
        public int Sharp { get; set; }
        public string Date { get; set; }
        public string Time { get; set; }
        public string Thread { get; set; }
        public string Message { get; set; }
    }
}
