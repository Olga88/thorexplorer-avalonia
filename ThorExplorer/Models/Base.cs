﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

namespace ThorExplorer.Models
{
    public class Base
    {
        public string BaseName { get; set; }

        public ObservableCollection<Engine> Engines { get; } 
            = new ObservableCollection<Engine>();

       
    }
}
