using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using ThorExplorer.Models;
using ThorExplorer.Models.ObjectsTable;

namespace ThorExplorer.ViewModels
{
    public class MainWindowViewModel : ViewModelBase
    {
        public ObservableCollection<Base> Bases { get; }
        public ObservableCollection<PersistentObject> Objects { get; }
        public ObservableCollection<SystemLog> SystemLogs { get; }


        public MainWindowViewModel()
        {
            Objects = new ObservableCollection<PersistentObject>(GenerateObjectsTable());
            Bases = new ObservableCollection<Base>(FillBase());
            
            SystemLogs = new ObservableCollection<SystemLog>(GenerateSystemLogTable());

        }

        private IEnumerable<SystemLog> GenerateSystemLogTable()
        {
            return new List<SystemLog>()
            {
                new SystemLog()
                {
                    TypeName = "Exception",
                    Sharp = 0,
                    Date = DateTime.Now.ToShortDateString(),
                    Time = DateTime.Now.ToLongTimeString(),
                    Thread = "Thread",
                    Message = "Check the file"
                }
            };
        }
     

        private IEnumerable<PersistentObject> GenerateObjectsTable()
        {
            var persistentObjects = new List<PersistentObject>()             
            {
                new PersistentObject()
                {
                    Identifier = "xxxxxxxx",
                    Description = "INP US Equity",
                    Type = "Stock"
                },
                new PersistentObject()
                {
                    Identifier = "xxxxxxxx",
                    Description = "INP US Equity",
                    Type = "Stock"
                },
                new PersistentObject()
                {
                    Identifier = "xxxxxxxx",
                    Description = "INP US Equity",
                    Type = "Stock"
                },
                new PersistentObject()
                {
                    Identifier = "xxxxxxxx",
                    Description = "INP US Equity",
                    Type = "Stock"
                }
                ,
                new PersistentObject()
                {
                    Identifier = "xxxxxxxx",
                    Description = "INP US Equity",
                    Type = "Stock"
                }
                ,
                new PersistentObject()
                {
                    Identifier = "xxxxxxxx",
                    Description = "INP US Equity",
                    Type = "Stock"
                }
                ,
                new PersistentObject()
                {
                    Identifier = "xxxxxxxx",
                    Description = "INP US Equity",
                    Type = "Stock"
                },
                new PersistentObject()
                {
                    Identifier = "xxxxxxxx",
                    Description = "INP US Equity",
                    Type = "Stock"
                },
                new PersistentObject()
                {
                    Identifier = "xxxxxxxx",
                    Description = "INP US Equity",
                    Type = "Stock"
                }

            };
            return persistentObjects;
        }

        private List<Base> FillBase()
        {
            return new List<Base>()
            {
                new Base()
                {
                    BaseName = "WS64-10.Thor",
                    Engines =
                    {
                        new Engine()
                        {
                            EngineName="SecurityEngine",
                            DataBases =
                            {
                                new DataBase(){DataBaseName = "Global"}
                            }
                        },
                        new Engine()
                        {
                            EngineName="OdbEngine",
                            DataBases =
                            {
                                new DataBase(){DataBaseName = "Global"},
                                new DataBase(){DataBaseName = "ODB1"}
                            }
                        },
                        new Engine()
                        {
                            EngineName="TdbEngine",
                            DataBases =
                            {
                                new DataBase(){DataBaseName = "Global"}
                            }
                        },
                        new Engine()
                        {
                            EngineName="ThorTableEngine"
                          
                        },
                        new Engine()
                        {
                            EngineName="Execution Engine",
                            DataBases =
                            {
                                new DataBase()
                                {
                                    DataBaseName = "PortfolioManager",
                                    Indices =
                                    {
                                        new Indexx(){IndexxName = "FuturesCn0_Live_Trade"}
                                    }
                                
                                }
                            }
                        }
                    }
                }
            };


        }
    }
}
